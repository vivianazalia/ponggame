﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour
{
    //Pemain yang akan bertambah skornya jika bola menyentuh dinidng ini
    public PlayerControl player;

    //Skrip Game Manager untuk mengakses skor maksimal
    [SerializeField]
    private GameManager gameManager;

    //Akan dipanggil ketika bola bersentuhan dengan dinding 
    private void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        //Jika objek tersebut bernama "Ball"
        if (anotherCollider.name == "Ball")
        {
            //Tambahkan skor ke pemain 
            player.IncrementScore();

            //Jika skor pemain belum mencapai skor maksimal...
            if (player.Score < gameManager.maxScore)
            {
                //...restart game jika mengenai dinding 
                anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
            }
        }
    }

    // Start is called before the first frame update
    /*void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }*/
}
